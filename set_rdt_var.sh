#!/bin/bash

# first , get the latest version number of both appium and droydrunner container

# IMPORTANT : the private TOKEN used to acces -READ ONLY- is belonging to ATAQ team 
# -s option is mandatory to avoid a download 'progress' status which would mess a ittle bit the script output





echo "************************************************************"
echo "************************************************************"
echo "*****                                                  *****"
echo "*****                                                  *****"
echo "*****         Robot global variable setup              *****"
echo "*****                 for RDT                          *****"
echo "*****                                                  *****"
echo "************************************************************"
echo "************************************************************"


whiptail --title "Robot global Var Setup for RDT" --yesno "This is the setup program which helps to set global variables in your robot test scripts \n\n\
5 main parameters must be configured : \n\
 - The IP adress of the RDT controler \n\
       note : the default is rdt.si.francetelecom.fr \n\
              but you may have to set it to 192.168.100.100 in the WWW\n\
 - The RDT Token you have created in RDT account on the web site\n\
       note : be ready to do a 'copy paste' of your token since it is very long ... \n\
 - the serial number of the mobile phone you want to use for your tests\n\
       note : the serial number is on the RDT web site , make also a 'copy paste' \n\
 - The way you access RDT API  with HTTPS or not \n\
       note : in the WWW, it should be True \n\
 - the directory where the program will search for files with .robot extension \n\
       note : the default is /home/live/Robot_Project/droyd_scripts \n\
 Do you want to go on ?\n\
                  " 25 90

if [ $? = 1 ] ; then
	clear
	echo " bye , have a nice day ... "
	exit 0
fi


sleep 1

clear

echo  "***************"
echo  "starting  form "
echo  "***************"


# c3ff7da3fc544f28b4c8c33c71df9d4b0f40f128273d43409d5f5b0fe341ed8f
# 495150932de7406e83a38067999ba6cb85c15666974841eb98229d11b5b48b54

if ping -c 2 www.google.com ; then
   echo " "
   echo "It seems you are in the WWW, the default IP address of the RDT server should be 192.168.100.100 "
   read -p "RDT Ip Address [192.160.100.100]: " RDT_Ip
   RDT_Ip=${RDT_Ip:-192.168.100.100}    
else
   echo " "
   echo "It seems you are in the RSC, the default IP address of the RDT server should be rdt.si.francetelecom.fr "
   read -p "RDT Ip Address [rdt.si.francetelecom.fr]: " RDT_Ip
   RDT_Ip=${RDT_Ip:-rdt.si.francetelecom.fr}    
fi

echo ""
echo "indicate your RDT token , the easiest is to make a 'copy' 'paste' "
read -p "RDT Token [from the web site]: " RDT_Token
echo ""
echo "indicate your Mobile Serial Number, the easiest is to make a 'copy' 'paste' "
read -p "Mobile Serial Number [from the web site]: " RDT_Serial
echo ""
echo "indicate if you use HTTPS to access RDT API : True or False "
read -p "RDT API HTTPS [True]: " RDT_Https
RDT_Https=${RDT_Https:-True}
echo ""
echo "indicate the directory where to look for robot files "
read -p "Robot files directory [/home/live/Robot_Project/droyd_scripts]: " RDT_Dir
RDT_Dir=${RDT_Dir:-/home/live/Robot_Project/droyd_scripts}
echo ""



if [ -z "$RDT_Ip" ]  ; then
	echo "RDT IP address not set , exiting , relaunch the setup script "
	echo "bye"
	exit 0
fi

if [ -z "$RDT_Token" ]  ; then
	echo "RDT Token not set , exiting , relaunch the setup script "
	echo "bye"
	exit 0
fi

if [ -z "$RDT_Serial" ]  ; then
	echo "RDT Serial not set , exiting , relaunch the setup script "
	echo "bye"
	exit 0
fi



if [ $RDT_Https != "True" ] && [ $RDT_Https != "False" ] ; then
	#statements
	echo "HTTPS not set , exiting , relaunch the setup script "
	echo "RDT Https : $RDT_Https"
	echo "bye"
	exit 0
fi

if [ ! -d "$RDT_Dir" ]  ; then
	echo "$RDT_Dir is not a directory , exiting , relaunch the setup script "
	echo "bye"
	exit 0
fi


filelist=$(ls $RDT_Dir/*.robot)

for file_id in $filelist; do
	sleep 1
	echo "processing $file_id"
#	sed -i -e 's/\^\$/\$/g' $file_id	
	sed -i -e "s/^\${IP_RDT_CTRL}        .*/\${IP_RDT_CTRL}          $RDT_Ip/g" $file_id
	sed -i -e "s/^\${UIID}      .*/\${UIID}                 $RDT_Serial/g" $file_id
	sed -i -e "s/^\${TOKEN_ACCESS_RDT}      .*/\${TOKEN_ACCESS_RDT}                    $RDT_Token/g" $file_id
	sed -i -e "s/^\${API_HTTPS}      .*/\${API_HTTPS}                    $RDT_Https/g" $file_id

#	sed -i -e 's/ disconnectMobile / LaaS.disconnectMobile /g' $file_id
done



