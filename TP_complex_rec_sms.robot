*** Settings ***

Library           Remote    http://${IP_CONTAINER_DROYDRUNNER}:${PORT_LAAS}     WITH NAME    LaaS
Library           droydclient.robot_plugin.Pilot
Library           Collections
Library           String

Resource    macro.robot

################################################
#  WARNING
#  export RF_HUB_URL=http://127.0.0.1:5001
#
################################################

*** Variables ***
${IP_CONTAINER_DROYDRUNNER}    127.0.0.1
${PORT_LAAS}                        5000


#${IP_RDT_CTRL}                 rdt.si.francetelecom.fr
# WARNING : IP du serveur RDT : rdt.si.francetelecom.fr si local RSC
# WARNING : sur le WWW : 192.168.100.100 , c'est la même en prod et dev, il s'agit de l'adresse interne au OPENVPN

${IP_RDT_CTRL}          192.168.100.100
${TOKEN_ACCESS_RDT}                    <MY TOKEN>
${API_HTTPS}                    True

#  WARNING : c'est le TOKEN qui a été généré sur la page du controler RDT une fois que l'on s'est connecté avec son login / mot de passe

${UIID}                 <MY SERIAL>

# WARNING : c'est le 'serial' du mobile que l'on souhaite utiliser pour le test



${timeout_mobile}              8000

# lancement : export RF_HUB_URL=http://laas_droyrunner:5001
#           : robot web.robot

${originphonenumber}              +336xxxxxxx



*** Test Cases ***
Test reception SMS
    Builtin.Log To Console    'get last received sms Droydrunner'

    # StartVPN
#    startOpenVPN    ${IP_RDT_CTRL}    ${USER_RDT}    ${PWD_RDT}

    # Add Adb Key to RDT Ctrl
    #addAdbKey    ${IP_RDT_CTRL}    ${EMAIL_RDT}

    # Connect mobile from RDT
    ${id_mobile}     LaaS.connectMobile    ${IP_RDT_CTRL}    ${TOKEN_ACCESS_RDT}    ${UIID}    ${API_HTTPS}

    Open Session    ${id_mobile}

    # raz de l'application messaging



    Execute Adb Shell Command    ${id_mobile}    cmd=am force-stop com.samsung.android.messaging
    Builtin.sleep    2


    # lancement d el'aplication messaging

    Execute Adb Shell Command    ${id_mobile}    cmd=monkey -p com.samsung.android.messaging -c android.intent.category.LAUNCHER 1
    # Builtin.sleep    5


    # Builtin.Log To Console    'Screenshot'
    # Screenshot     ${id_mobile}
    # Builtin.Log To Console    'end of Screenshot'

    Builtin.Log To Console    'wait for exists'

    # wait for the LinearLayout
#    ${tmp_result}    Wait For Exists    ${id_mobile}    resourceId=com.samsung.android.messaging:id/from    textStartsWith=${originphonenumber}    timeout=10
    ${tmp_result}    Wait For Exists    ${id_mobile}    classNameMatches=.*TextView    textStartsWith=${originphonenumber}    timeout=10

    Builtin.Log To Console    'wait for exists result : '${tmp_result}

    Builtin.Log To Console    'Screenshot'
    Screenshot    ${id_mobile}

    # click on the mobile phone number
    # click    ${id_mobile}     resourceId=com.samsung.android.messaging:id/from      textStartsWith=⁨${originphonenumber}
    click    ${id_mobile}     classNameMatches=.*TextView       textStartsWith=⁨${originphonenumber}

    Builtin.sleep    2


    # count the number of messages in the current page
    ${nb_msg}    droydclient.robot_plugin.Pilot.Get Count    ${id_mobile}    resourceId=com.samsung.android.messaging:id/list_item_text_view    className=android.widget.TextView

    Builtin.Log To Console    'number of messages : '${nb_msg}

    Builtin.sleep    2

    # compute the msg index of the last msg
    ${idMsg}    evaluate    ${nb_msg}-1

    # ${obj_list_msg}    Get Object    ${id_mobile}    resourceId=com.samsung.android.messaging:id/base_list_item_data    className=android.widget.LinearLayout

    # ${info_list_msg}    Get Info Of Object    ${id_mobile}    obj=${obj_list_msg}

    # Builtin.Log To Console    'info list msg'
    # Builtin.Log To Console    ${info_list_msg}
    # ${textMsg}    evaluate    ${obj_list_msg}.get('contentDescription')
    #Builtin.Log To Console    'text list msg'
    #Builtin.Log To Console    ${textMsg}

    Builtin.sleep    2

    # ${obj_msg}    Get Object    ${id_mobile}    className=android.widget.LinearLayout    index=${idMsg}
    # #Builtin.Log To Console    'obj msg'
    # #Builtin.Log To Console    ${obj_msg}
    # ${info_msg}    Get Info Of Object    ${id_mobile}    obj=${obj_msg}
    # Builtin.Log To Console    'info msg'
    # Builtin.Log To Console    ${info_msg}

    # Builtin.sleep    3

    # it is mandatory to use instance UISelector since index is not assigned ...
    Builtin.Log To Console    'get msg object'
    ${obj_msg}    Get Object    ${id_mobile}    resourceId=com.samsung.android.messaging:id/list_item_text_view    className=android.widget.TextView    instance=${idMsg}




    # ${child_Msg}    Get Child    ${id_mobile}    obj=${obj_msg}    resourceId=com.samsung.android.messaging:id/list_item_text_view    # index=0
    ${info_msg}    Get Info Of Object    ${id_mobile}    obj=${obj_msg}
    #${child_child_Msg}    Get Child    ${id_mobile}    obj=${child_Msg}    className=LinearLayout
    #${info_child_child_Msg}    Get Info Of Object    ${id_mobile}    obj=${child_child_Msg}
#    ${info_msg}    Get Info Of Object    ${id_mobile}    obj=${child_child_Msg}

    Builtin.Log To Console    'info object msg'
    Builtin.Log To Console    ${info_msg}
    #Builtin.Log To Console    ${info_child_child_Msg}

    # Builtin.Log To Console    'info msg'
    # Builtin.Log To Console    ${info_msg}
    ${textMsg}    evaluate    ${info_msg}.get("text")
    Builtin.Log To Console    'text msg'
    Builtin.Log To Console    ${textMsg}


    [Teardown]    TestTeardown    mob=${UIID}     id_mobile=${id_mobile}

*** Keywords ***
TestTeardown
    [arguments]     ${mob}     ${id_mobile}
    [Documentation]     TestTeardown
    Builtin.Log To Console    'TestTeardown'
    Screenshot      ${id_mobile}
    Close Session
    LaaS.disconnectMobile    ${IP_RDT_CTRL}    ${TOKEN_ACCESS_RDT}    ${mob}    ${API_HTTPS}









