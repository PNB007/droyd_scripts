*** Settings ***
#include the droydrunner library

Library     Remote    http://${IP_CONTAINER_DROYDRUNNER}:${PORT_LAAS}     WITH NAME    LaaS
Library     droydclient.robot_plugin.Pilot
Library     Collections
Library     String

*** Variables ***


${IP_CONTAINER_DROYDRUNNER}    localhost
${PORT_LAAS}                        5000

${IP_RDT_CTRL}          192.168.100.100
# WARNING : IP du serveur RDT : rdt.si.francetelecom.fr si local RSC
# WARNING : sur le WWW : 192.168.100.100 , c'est la même en prod et dev, il s'agit de l'adresse interne au OPENVPN
${TOKEN_ACCESS_RDT}                    <MY TOKEN>
${API_HTTPS}                    True

# ${UIID}                        WARNING : c'est le 'serial' du mobile que l'on souhaite utiliser pour le test
${UIID}                 <MY SERIAL>
${timeout_mobile}              8000


# Exemple possible


# ?? ${id_mobile}      10.179.8.61:15061


*** Test Cases ***

Test SMS Receive
    Builtin.Log To Console    'Test SMS Receive'

     # Connect mobile from RDT
    ${id_mobile}     LaaS.connectMobile    ${IP_RDT_CTRL}    ${TOKEN_ACCESS_RDT}    ${UIID}

    Open Session    ${id_mobile}

    # Fermeture puis ouverture des SMS
    Builtin.Log To Console    'Force Stop MMS App'
    Execute Adb Shell Command    ${id_mobile}    cmd=am force-stop com.samsung.android.messaging
    Builtin.sleep    2
    Builtin.Log To Console    'Force Start MMS App'
    Execute Adb Shell Command    ${id_mobile}    cmd=am start -p com.samsung.android.messaging 1 -W
    Builtin.sleep    2

    Click    ${id_mobile}    resourceId=com.samsung.android.messaging:id/conversationList_item_root
    Builtin.sleep    2

    ${getphone}    Get Object    ${id_mobile}    resourceId=com.samsung.android.messaging:id/actionbar_compose_message_list_title
    ${getphone}    Get Info Of Object    ${id_mobile}    obj=${getphone}
    ${numPhoneExp}    evaluate    ${getphone}.get("text")
    Builtin.Log To Console    /// Numero expediteur
    Builtin.Log To Console    ${numPhoneExp}

    # Get Count Msg
    ${getHistory}    Get Object    ${id_mobile}    resourceId=com.samsung.android.messaging:id/history
    ${getListMsg}    Get Info Of Object    ${id_mobile}    obj=${getHistory}
    #Builtin.Log To Console    ${getListMsg}
    ${countMsg}    evaluate    ${getListMsg}.get("childCount")
    Builtin.Log To Console    /// Nombre de message
    Builtin.Log To Console    ${countMsg}

    # Get Last Msg
    ${idMsg}    evaluate    ${countMsg}-1
 #   ${objMsg}    Get Object    ${id_mobile}    resourceId=com.android.mms:id/base_list_item_data    instance=${idMsg}
    ${objMsg}    Get Object    ${id_mobile}    resourceId=com.samsung.android.messaging:id/base_list_item_data    instance=${idMsg}
    #Builtin.Log To Console    ///////////////// MSG1
    #Builtin.Log To Console    ${objMsg}

    # Get Content
    ${contentMsg}    Get Child    ${id_mobile}    obj=${objMsg}    resourceId=com.samsung.android.messaging:id/list_item_text_view
    #Builtin.Log To Console    ///////////////// MSG2
    #Builtin.Log To Console    ${contentMsg}
    ${textMsg}    evaluate    ${contentMsg}.get("text")
    Builtin.Log To Console    /// Message complet
    Builtin.Log To Console    ${textMsg}

#    Close Session

  [Teardown]    TeardownAll

*** Keywords ***
TeardownAll
    [arguments]

    LaaS.disconnectMobile    ${IP_RDT_CTRL}    ${TOKEN_ACCESS_RDT}    ${UIID}
    Close Session












