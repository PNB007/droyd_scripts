*** Settings ***

Library           Remote    http://${IP_CONTAINER_DROYDRUNNER}:${PORT_LAAS}     WITH NAME    LaaS
Library           droydclient.robot_plugin.Pilot
Library           Collections
Library           String

Resource           macro.robot

*** Variables ***


${IP_CONTAINER_DROYDRUNNER}    127.0.0.1
${PORT_LAAS}                        5000

${IP_RDT_CTRL}          192.168.100.100
#${IP_RDT_CTRL}                 rdt.si.francetelecom.fr
# WARNING : IP du serveur RDT : rdt.si.francetelecom.fr si local RSC
# WARNING : sur le WWW : 192.168.100.100 , c'est la même en prod et dev, il s'agit de l'adresse interne au OPENVPN
${TOKEN_ACCESS_RDT}                    <MY TOKEN>
${API_HTTPS}                    True

${UIID}                 <MY SERIAL>


${timeout_mobile}              8000

# lancement : export RF_HUB_URL=http://laas_droyrunner:5001
#           : robot web.robot

*** Test Cases ***
Test Navigation Web Search
    Builtin.Log To Console    'Test Navigation Web Search Droydrunner'

    # StartVPN
#    startOpenVPN    ${IP_RDT_CTRL}    ${USER_RDT}    ${PWD_RDT}

    # Add Adb Key to RDT Ctrl
    #addAdbKey    ${IP_RDT_CTRL}    ${EMAIL_RDT}

    # Connect mobile from RDT
    ${id_mobile}     LaaS.connectMobile    ${IP_RDT_CTRL}    ${TOKEN_ACCESS_RDT}    ${UIID}     ${API_HTTPS}

    Log To Console
    ...    "Id Mobile : "${id_mobile}

    Open Session    ${id_mobile}
    # press home Key
    Macro Press Home Key    ${id_mobile}

    # click on google search
    Click
    ...    ${id_mobile}
    ...    resourceId=com.google.android.googlequicksearchbox:id/hint_text_alignment

    Set Text
    ...     ${id_mobile}
    ...     resourceId=com.google.android.googlequicksearchbox:id/search_box      input_text=robotframework

    # press the enter key
    Macro Press Key
    ...    ${id_mobile}
    ...    66

    # BuiltIn.Sleep
    # ...    10

    Wait For Exists
    ...    ${id_mobile}
    ...    timeout=15     className=android.view.View     textContains=https://robotframework.org

    Click
    ...    ${id_mobile}
    ...    className=android.view.View     textContains=https://robotframework.org


    Builtin.Sleep    5






    [Teardown]    TestTeardown    mob=${UIID}

*** Keywords ***
TestTeardown
    [arguments]     ${mob}
    [Documentation]     TestTeardown
    Builtin.Log To Console    'TestTeardown'
    Close Session
    LaaS.disconnectMobile    ${IP_RDT_CTRL}    ${TOKEN_ACCESS_RDT}    ${mob}    ${API_HTTPS}
    stopOpenVPN    ${IP_RDT_CTRL}







