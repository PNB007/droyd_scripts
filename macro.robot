*** Settings ***
Documentation     demo for droydhub
    ...
    ...  export RF_HUB_URL=http://hub.com:5000
    ...  pybot -L trace -P ../ rf_native_hub.txt
    ...

# Library 	droydclient.robot_plugin.Pilot
# Library     Collections

*** Variables ***
# ${Alice}=    e7f54be6
# ${Bob}=    388897e5

# ${Alice_number}=     0684820364
# ${Bob_number}=       0640412593

*** Keywords ***


#
#  adb macros
#
Macro adb devices
   [Documentation]     unlock screen if necessary

    ${devices}  Adb devices
    Log List    ${devices}
    return from Keyword     ${devices}


#
#   general device macros
#

Macro Unlock Screen
    [arguments]     ${user}
    [Documentation]     unlock screen if necessary

    Turn On Screen  ${user}
    # get current screen
    ${device_info}  Get Device Info     ${user}
    #Log Dictionary  ${device_info}
    ${package_name}     Get From Dictionary     ${device_info}  currentPackageName
    Log     ${package_name}
    # return if not keyguard screen
    Return From Keyword If    '${package_name}' != 'com.android.keyguard'
    # swipe to unlock device
    Swipe By Coordinates   ${user}     sx=270     sy=1440    ex=800     ey=1000    steps=50


Macro Home
    [arguments]     ${user}
    [Documentation]     return to home screen
    Macro Unlock Screen     ${user}
    Press Home   ${user}
    Press Home   ${user}


Macro Press Key
    [arguments]     ${user}      ${keyvalue}
    [Documentation]     send a predefined key to the mobile
    # Macro Unlock Screen     ${user}
    Execute Adb Shell Command    ${user}    cmd=input keyevent ${keyvalue}


Macro Input Text
    [arguments]     ${user}      ${my_text}
    [Documentation]     send a predefined key to the mobile
    # Macro Unlock Screen     ${user}
    Execute Adb Shell Command    ${user}    cmd=input text ${my_text}



Macro Launch Package with activity
    [arguments]     ${user}     ${packageid}
    [Documentation]     launch phone application from home screen with hot key

    BuiltIn.Log To Console
    ...    "launcch package mobile id "${user}

    BuiltIn.Log To Console
    ...    "launch package id "${packageid}

    ${outputcmd}    Execute Adb Shell Command   ${user}     cmd="am start -n ${packageid}"

    BuiltIn.Log To Console
    ...    "launch package id output "${outputcmd}

Macro Stop Running Package
    [arguments]     ${user}     ${packageid}

    BuiltIn.Log To Console
    ...    "stop running package mobile id "${user}

    BuiltIn.Log To Console
    ...    "stop running package package id "${packageid}

    BuiltIn.Sleep
    ...    2


    [Documentation]     launch phone application from home screen with hot key
    Execute Adb Shell Command   ${user}     cmd=am force-stop ${packageid}


Macro Quick Launch Phone
    [arguments]     ${user}
    [Documentation]     launch phone application from home screen with hot key
    click   ${user}  className=android.widget.TextView     packageName=com.sec.android.app.launcher    text=Phone


Macro Get airplane mode
    [arguments]     ${user}
    [Documentation]     put device in airplane mode (if necessary)
    # goto airplane settings

    ${airplane}     Execute Adb Shell Command    ${user}    cmd=settings get global airplane_mode_on
    Builtin.Log To Console    'airplane mode'
    Builtin.Log To Console    ${airplane}
    Builtin.Log To Console    ${airplane}[0]
    ${airplaneset}    evaluate    1 == ${airplane}[0]
    Builtin.Log To Console    'airplane set'
    Builtin.Log To Console    ${airplaneset}

    [Return]      ${airplaneset}



Macro Settings airplane on
    [arguments]     ${user}
    [Documentation]     put device in airplane mode (if necessary)
    # goto airplane settings
    # again we use android available commands , very efficient

    ${airplaneON}      Macro Get airplane mode    ${user}

    Builtin.Log To Console    'airplaneON'
    Builtin.Log To Console    ${airplaneON}

    return from keyword if    ${airplaneON}==True

    Builtin.Log To Console    'airplane mode is not set, let us do it'

    Execute Adb Shell Command    ${user}    cmd=am start -a android.settings.AIRPLANE_MODE_SETTINGS

    Builtin.sleep    2
    ${result}   Wait for Exists     ${user}    timeout=4     resourceId=com.android.settings:id/switch_widget
    # click on airplane logo
    click   ${user}     resourceId=com.android.settings:id/switch_widget
    # press keycode 4 which means 'back'
    Execute Adb Shell Command     ${user}    cmd=input keyevent 4



Macro Settings airplane off
    [arguments]     ${user}
    [Documentation]     put device in airplane mode (if necessary)
    # goto airplane settings
    # again we use android available commands , very efficient

    ${airplaneON}      Macro Get airplane mode    ${user}

    Builtin.Log To Console    'airplaneON'
    Builtin.Log To Console    ${airplaneON}

    return from keyword if    ${airplaneON}==False

    Builtin.Log To Console    'airplane mode is set, clear it'

    Execute Adb Shell Command    ${user}    cmd=am start -a android.settings.AIRPLANE_MODE_SETTINGS

    Builtin.sleep    2
    ${result}   Wait for Exists     ${user}    timeout=4     resourceId=com.android.settings:id/switch_widget
    # click on airplane logo
    click   ${user}     resourceId=com.android.settings:id/switch_widget
    # press keycode 4 which means 'back'
    Execute Adb Shell Command     ${user}    cmd=input keyevent 4


Macro Press Home Key
    [arguments]     ${MobileUnderTest}
    # press keycode 4 which means 'back'
    Execute Adb Shell Command     ${MobileUnderTest}    cmd=input keyevent 3

Macro Send SMS to Short
    [arguments]     ${MobileUnderTest}     ${destNbr}      ${msgtext}
    [Documentation]     send a SMS to destNbr

    Execute Adb Shell Command    ${MobileUnderTest}    cmd=am force-stop com.samsung.android.messaging
    Builtin.sleep    2


    # Launch SMS/MMS app and start new SMS with ${phonenumber}
    Execute Adb Shell Command    ${MobileUnderTest}    cmd=am start -a android.intent.action.SENDTO -d sms:${destNbr}
    Builtin.sleep    2

    ${result}   Wait for Exists     ${MobileUnderTest}    timeout=4     resourceId=com.samsung.android.messaging:id/editor_body

    Set Text    ${MobileUnderTest}    input_text=${msgtext}    resourceId=com.samsung.android.messaging:id/editor_body

    Click    ${MobileUnderTest}    resourceId=com.samsung.android.messaging:id/send_button

    Macro Press Home Key     ${MobileUnderTest}
    Builtin.sleep    2


Macro Send SMS to Long
    [arguments]     ${MobileUnderTest}     ${destNbr}      ${msgtext}
    [Documentation]     send a SMS to destNbr

    Execute Adb Shell Command    ${MobileUnderTest}    cmd=am force-stop com.samsung.android.messaging
    Builtin.sleep    2


    #  launch messaging"
    Execute Adb Shell Command    ${MobileUnderTest}    cmd=monkey -p com.samsung.android.messaging -c android.intent.category.LAUNCHER 1
    # click to write a message
    Click    ${MobileUnderTest}    resourceId=com.samsung.android.messaging:id/floating_action_button     index=2

    Builtin.sleep    2

    # click on 'rediger button'

    Click    ${MobileUnderTest}    resourceId=com.samsung.android.messaging:id/btn_menu_compose     textContains=DIGER

    Builtin.sleep    2
    # set destination number
    ${result}   Wait for Exists     ${MobileUnderTest}    timeout=4     resourceId=com.samsung.android.messaging:id/recipients_editor_to
    Set Text    ${MobileUnderTest}    input_text=${destNbr}    resourceId=com.samsung.android.messaging:id/recipients_editor_to      textContains=Destinataire

    # set text to send_button
    Builtin.sleep    2
    ${result}   Wait for Exists     ${MobileUnderTest}    timeout=4     resourceId=com.samsung.android.messaging:id/editor_body
    Set Text    ${MobileUnderTest}    input_text=${msgtext}    resourceId=com.samsung.android.messaging:id/editor_body      textContains=message

    Builtin.sleep    2
    # click to send message
    Click    ${MobileUnderTest}    resourceId=com.samsung.android.messaging:id/send_button     textContains=ENVOI

    Builtin.sleep    2
    # stop messaging service

    Macro Press Home Key     ${MobileUnderTest}
    Builtin.sleep    2



# mobile phone is ringing, someone is calling
# adb shell dumpsys telephony.registry | grep CallState
#     mCallState=1
#   mPreciseCallState=Ringing call state: 5, Foreground call state: 0, Background call state: 0, Disconnect cause: -1, Precise disconnect cause: -1
#   mRingingCallState=5
#   mForegroundCallState=0
#   mBackgroundCallState=0

#mobile phone is calling, called party is ringing
# adb shell dumpsys telephony.registry | grep CallState
#     mCallState=2
#   mPreciseCallState=Ringing call state: 0, Foreground call state: 4, Background call state: 0, Disconnect cause: -1, Precise disconnect cause: -1
#   mRingingCallState=0
#   mForegroundCallState=4
#   mBackgroundCallState=0


# mobile phone is in communication
# adb shell dumpsys telephony.registry | grep CallState
#     mCallState=2
#   mPreciseCallState=Ringing call state: 0, Foreground call state: 1, Background call state: 0, Disconnect cause: -1, Precise disconnect cause: -1
#   mRingingCallState=0
#   mForegroundCallState=1
#   mBackgroundCallState=0

# mobile phone is idle
# adb shell dumpsys telephony.registry | grep CallState
#     mCallState=0
#   mPreciseCallState=Ringing call state: 0, Foreground call state: 0, Background call state: 0, Disconnect cause: -1, Precise disconnect cause: -1
#   mRingingCallState=0
#   mForegroundCallState=0
#   mBackgroundCallState=0


    # Set SMS text

    # # wait for popup with text: Turn On Airplane mode
    # ${pop_up}   Wait for Exists     ${user}    timeout=1   text=Turn on Airplane mode  packageName=com.android.systemui  className=android.widget.TextView  resourceId=android:id/alertTitle
    # # click on OK
    # run keyword if  ${pop_up} == True  click   ${user}  packageName=com.android.systemui  className=android.widget.Button  text=OK
    # # click on CANCEL
    # run keyword if  ${pop_up} == False  click   ${user}  packageName=com.android.systemui  className=android.widget.Button  text=Cancel

#
#  telephony macros
#

Macro Wait Incoming Call Ringing Keyword

    [arguments]     ${MobileUnderTest}     ${destNbr}


    ${result}      Wait for exists     ${MobileUnderTest}       timeout=30     resourceId=com.samsung.android.incallui:id/callStateLabel       textContains=entrant
    screenshot     ${MobileUnderTest}

    Builtin.Log To Console    ${result}



Macro Incoming Call Ringing Adb
    [arguments]     ${MobileUnderTest}

    ${mCallStateList}     Execute Adb Shell Command    ${MobileUnderTest}    cmd='dumpsys telephony.registry | grep mCallState'
    ${mRingingCallStateList}     Execute Adb Shell Command    ${MobileUnderTest}    cmd='dumpsys telephony.registry | grep mRingingCallState'

    #Builtin.Log To Console    'mCallState :  '
    Builtin.Log To Console    ${mCallStateList}[0]

    #Builtin.Log To Console    'mForegroundCallState :  '
    Builtin.Log To Console    ${mRingingCallStateList}[0]


    Should Contain     ${mCallStateList}[0]     1
    Should Contain     ${mRingingCallStateList}[0]     5


Macro Wait Incoming Call Ringing Adb

    [arguments]     ${MobileUnderTest}     ${destNbr}
    [Documentation]     launch a call to dest number

    Wait Until Keyword Succeeds     4x     6s      Macro Incoming call Ringing Adb       ${MobileUnderTest}

    Macro Check Calling Number
    ...    ${MobileUnderTest}
    ...    ${destNbr}



Macro Call established
    [arguments]     ${MobileUnderTest}

    ${mCallStateList}     Execute Adb Shell Command    ${MobileUnderTest}    cmd='dumpsys telephony.registry | grep mCallState'
    ${mForegroundCallStateList}     Execute Adb Shell Command    ${MobileUnderTest}    cmd='dumpsys telephony.registry | grep mForegroundCallState'

    #Builtin.Log To Console    'mCallState :  '
    Builtin.Log To Console    ${mCallStateList}[0]

    #Builtin.Log To Console    'mForegroundCallState :  '
    Builtin.Log To Console    ${mForegroundCallStateList}[0]


    Should Contain     ${mCallStateList}[0]     2
    Should Contain     ${mForegroundCallStateList}[0]     1




Macro Make a Call
    [arguments]     ${MobileUnderTest}     ${destNbr}
    [Documentation]     launch a call to dest number

    Execute Adb Shell Command    ${MobileUnderTest}    cmd=am start -a android.intent.action.CALL -d "tel:${destNbr}"

    Builtin.sleep     4

    Wait Until Keyword Succeeds     4x     6s      Macro Call established       ${MobileUnderTest}

    Log To Console
    ...    Call established yeah !!  Wait 6 seconds

    BuiltIn.Sleep
    ...    6
    ...    noneed

    Macro Press Key     ${MobileUnderTest}     KEYCODE_ENDCALL

    Macro Press Key     ${MobileUnderTest}     KEYCODE_HOME


    BuiltIn.Sleep
    ...    6
    ...    noneed


    # ${mCallState}     Execute Adb Shell Command    ${MobileUnderTest}    cmd='dumpsys telephony.registry | grep mCallState'
    # ${mForegroundCallState}     Execute Adb Shell Command    ${MobileUnderTest}    cmd='dumpsys telephony.registry | grep mForegroundCallState'
    # Builtin.Log To Console    'mCallState :  '
    # Builtin.Log To Console    ${mCallState}[0]
    # Builtin.Log To Console    'mForegroundCallState :  '
    # Builtin.Log To Console    ${mForegroundCallState}[0]


    # Builtin.sleep     4


    # ${mCallState}     Execute Adb Shell Command    ${MobileUnderTest}    cmd='dumpsys telephony.registry | grep mCallState'
    # ${mForegroundCallState}     Execute Adb Shell Command    ${MobileUnderTest}    cmd='dumpsys telephony.registry | grep mForegroundCallState'
    # Builtin.Log To Console    'mCallState :  '
    # Builtin.Log To Console    ${mCallState}[0]
    # Builtin.Log To Console    'mForegroundCallState :  '
    # Builtin.Log To Console    ${mForegroundCallState}[0]

Macro Check Calling Number
    [arguments]     ${MobileUnderTest}      ${MobilePhone}


    ${IncomingNumberList}     Execute Adb Shell Command    ${MobileUnderTest}    cmd=dumpsys activity | grep -i incoming_number | grep -o -m1 -E '[+|0-9]{4,15}'


    #Builtin.Log To Console    'mCallState :  '
    BuiltIn.Log To Console
    ...    'result of dumpsys activity'
    Builtin.Log To Console    ${IncomingNumberList}


    Should Contain
    ...    ${IncomingNumberList}[0]
    ...    ${MobilePhone}





Macro Phone Keypad
    [documentation]    select keypad in phone application
    [arguments]     ${user}
    #return self.device(className='android.app.ActionBar$Tab',packageName=self.package_name,index=index).click()
    click   ${user}     className=android.app.ActionBar$Tab   packageName=com.android.contacts   index=0

Macro Phone dial number
    [Documentation]     dial number when in phone.keypad
    [arguments]     ${user}     ${number}

    # long click on text zone
    long click   ${user}    resourceId=com.android.contacts:id/digits
    #  click delete button
    click   ${user}    resourceId=com.android.contacts:id/deleteButton
    # set text with number
    set text    ${user}   input_text=${number}   resourceId=com.android.contacts:id/digits
    # press dial button
    click   ${user}    resourceId=com.android.contacts:id/dialButton


# Macro Phone is incoming call

#     [Documentation]     is incoming pop up present
#     [arguments]     ${user}

#     #exists   ${user}   resourceId=com.android.incallui:id/


Macro Phone wait incoming call

    [Documentation]     wait for an incoming call
    [arguments]     ${user}     ${timeout}=10000

    ${call}     Wait For Exists   ${user}   timeout=${timeout}  resourceId=com.android.incallui:id/popup_call_state
    Return From Keyword If    ${call} == True
    Fail    msg=no incoming call received

Macro Phone answer call

    [Documentation]     click answer button in callui popup
    [arguments]     ${user}

    click   ${user}     resourceId=com.android.incallui:id/popup_call_answer

Macro Phone reject call

    [Documentation]     click CANCEL button in callui popup
    [arguments]     ${user}

     click   ${user}     resourceId=com.android.incallui:id/popup_call_reject

Macro Phone hangup call

    [Documentation]     click Hangup in incallui
    [arguments]     ${user}

     click   ${user}     resourceId=com.android.incallui:id/endButton
