*** Settings ***
#include the droydrunner library
Library     droydclient.robot_plugin.Pilot
Library     Remote    http://${IP_CONTAINER_DROYDRUNNER}:${PORT_LAAS}     WITH NAME    LaaS
Library     Collections
Library     String

Resource    macro.robot

*** Variables ***

################################################
#  WARNING
#  export RF_HUB_URL=http://127.0.0.1:5001
#
################################################




${IP_CONTAINER_DROYDRUNNER}    127.0.0.1
${PORT_LAAS}                        5000

${IP_RDT_CTRL}          192.168.100.100
# WARNING : IP du serveur RDT : rdt.si.francetelecom.fr si local RSC
# WARNING : sur le WWW : 192.168.100.100 , c'est la même en prod et dev, il s'agit de l'adresse interne au OPENVPN
${TOKEN_ACCESS_RDT}                    <MY TOKEN>
${API_HTTPS}                    True

# ${UIID}                        WARNING : c'est le 'serial' du mobile que l'on souhaite utiliser pour le test
${UIID}                 <MY SERIAL>

${timeout_mobile}              8000

${phonenumber}              +336xxxxxxxx
${text}                     SMS sent by Robot Framework with LAAS Droydrunner !
${second_text}             "Another SMS sent by Robot Framework with LAAS Droydrunner"

*** Test Cases ***

Outgoing Call With ADB
    Builtin.Log To Console    'Outgoing Call with ADB'

    # Connect mobile from RDT
    ${id_mobile}     LaaS.connectMobile    ${IP_RDT_CTRL}    ${TOKEN_ACCESS_RDT}    ${UIID}     ${API_HTTPS}

    Builtin.Log To Console    'id mobile sent back by connectmobile '
    Builtin.Log To Console    ${id_mobile}

    Open Session    ${id_mobile}


    BuiltIn.Sleep    6


    Macro Make a Call      ${id_mobile}      ${phonenumber}



    [Teardown]    TeardownAll

*** Keywords ***
TeardownAll
    [arguments]

    LaaS.disconnectMobile    ${IP_RDT_CTRL}    ${TOKEN_ACCESS_RDT}    ${UIID}    True
    Close Session






