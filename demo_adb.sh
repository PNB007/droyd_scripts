#!/bin/bash



echo "************************************************************"
echo "************************************************************"
echo "*****                                                  *****"
echo "*****                                                  *****"
echo "*****         ADB : Android Debug simple demo          *****"
echo "*****                                                  *****"
echo "*****                                                  *****"
echo "*****                                                  *****"
echo "************************************************************"
echo "************************************************************"

sleep 2

if  [ !  -f /home/$USER/bin/adb_form.tmp ]; then

	whiptail --title "ADB demo script" --yesno "This small script will show you few examples of what you can do with ADB \n\n\
	This program will ask you 3 inputs \n\
	3 main parameters must be configured : \n\
	 - the IP and PORT of the mobile you want to connect to \n\
	   note : you will find this information on the 'control' page on RDT server  \n\
	 - the destination number you want to send a SMS to and Call \n\
	 - the URL you want to open with your browser \n\
	 These are just simple actions but you can do also complex actions\n\
	 Do you want to go on ?\n\
	                  " 20 80

	if [ $? = 1 ] ; then
		clear
		echo " bye , have a nice day ... "
		exit 0
	fi




fi




if [ -f /home/$USER/bin/adb_form.tmp ]; then

Current_ID=`sed -n 1p /home/$USER/bin/adb_form.tmp`
Current_Nbr=`sed -n 2p /home/$USER/bin/adb_form.tmp`
Current_Url=`sed -n 3p /home/$USER/bin/adb_form.tmp`

else
	#statements
Current_ID="<IP>:<Port>"
Current_Nbr="06XXXXXXXX"
Current_Url="www.orange.fr"

fi

dialog --backtitle "ADB demo Setup " --title "Form" \
--form "use <tab> and <arrow> to move" 25 60 16 \
"mobile IP:Port :" 1 1 $Current_ID  1 25 30 30  \
"Destination Number :" 2 1 $Current_Nbr 2 25 30 30  \
"URL :" 3 1 $Current_Url 3 25 30 30   > /home/$USER/bin/adb_form.tmp \
2>&1 >/dev/tty

exitcode=$?

clear

if [ $exitcode = 1 ] ; then
	clear
	echo " bye , have a nice day ... "
	exit 0
fi

Current_ID=`sed -n 1p /home/$USER/bin/adb_form.tmp`
Current_Nbr=`sed -n 2p /home/$USER/bin/adb_form.tmp`
Current_Url=`sed -n 3p /home/$USER/bin/adb_form.tmp`



if [ -z "$Current_ID" ]  ; then
	echo "IP Port of the mobile not set , exiting , relaunch the setup script "
	echo "bye"
	exit 0
fi

if [ -z "$Current_Nbr" ]  ; then
	echo "Destination number not set , exiting , relaunch the setup script "
	echo "bye"
	exit 0
fi

if [ -z "$Current_Url" ]  ; then
	echo "URL not set , exiting , relaunch the setup script "
	echo "bye"
	exit 0
fi


# if [[ $RDT_IP =~ [0-9]{1-3}.[0-9]{1-3}.[0-9]{1-3}.[0-9]{1-3} ]] ; then 

# if [[ ! $RDT_IP =~ [0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3} ]] ; then 
# 	echo "IP address invalid"
# 	echo "bye"
# 	exit 0	
# fi 


adb connect $Current_ID
sleep 2
adb devices
sleep 2

echo " ********************** "
echo " switch off the screen  "
echo " ********************** "

adb shell input keyevent KEYCODE_POWER

sleep 2

echo " ********************* "
echo " switch on the screen  "
echo " ********************* "

adb shell input keyevent KEYCODE_POWER

sleep 2

echo " *********** "
echo " go to Home  "
echo " *********** "

adb shell input keyevent KEYCODE_HOME


sleep 2

echo " ************** "
echo " launch camera  "
echo " ************** "

adb shell "am start -a android.media.action.IMAGE_CAPTURE"

sleep 5

echo " *********** WARNING ***********"
echo " take a picture  (it could fail on mobiles with different screen size ) "
echo " *********** WARNING ***********"

adb shell input tap 550  1750

sleep 5

echo " *********** "
echo " go to Home  "
echo " *********** "

adb shell input keyevent KEYCODE_HOME

sleep 4

echo " *********** "
echo " send a SMS  "
echo " *********** "


adb shell am start -a android.intent.action.SENDTO -d sms:$Current_Nbr --es sms_body "Hello\ Adb\ Player" --ez exit_on_sent true

sleep 4

echo " *********** WARNING ***********"
echo " next command which click on 'send sms' could fail depending on the screen size "
echo " *********** WARNING ***********"


adb shell input tap 1000 1850

sleep 5

echo " *********** "
echo " go to Home  "
echo " *********** "

adb shell input keyevent KEYCODE_HOME

sleep 5

echo " *********** "
echo " make a call  "
echo " *********** "

adb shell am start -a "android.intent.action.CALL" -d "tel:$Current_Nbr"

callactive=no

while [ ! $callactive = "yes" ]; do
	mCallState=$(adb shell dumpsys telephony.registry | grep mCallState)
	echo " mCallState : $mCallState "
	mForegroundCallState=$(adb shell dumpsys telephony.registry | grep mForegroundCallState)
	echo " mForegroundCallState : $mForegroundCallState "	
	if [ $mCallState = "mCallState=2" ] && [ $mForegroundCallState =  "mForegroundCallState=1" ]; then
		callactive=yes
		echo " "
		echo " ******************************** "
		echo " Great !! , the call is active !! "
		echo " ******************************** "
		echo " "
	fi
	echo "callactive : $callactive "
	sleep 3
done

sleep 5


echo " *********** "
echo " clear call  "
echo " *********** "

adb shell input keyevent  KEYCODE_ENDCALL

sleep 4

echo " *********** "
echo " go to Home  "
echo " *********** "

adb shell input keyevent KEYCODE_HOME

sleep 4

echo " ************************ "
echo " open browser with a URL  "
echo " ************************ "

adb shell am start -a "android.intent.action.VIEW" -d "http://$Current_Url"

sleep 10

echo " *********** "
echo " go to Home  "
echo " *********** "

adb shell input keyevent KEYCODE_HOME

echo " ********************** "
echo " end of this short demo"
echo " ********************** "
