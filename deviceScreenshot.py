#!/usr/bin/python2.7

"""
Usage : python deviceScreenshot.py #idDuMobile
#idDuMobile est l identifiant du mobile retourne par la commande : adb devices
Si erreur sur : /from uiautomator import Device/ executer la commande : sudo pip install uiautomator
"""

from uiautomator import Device
import sys, time

def capture (deviceID):
    device = Device(deviceID)
    tstamp = "%s_%s.%s.%s-%sh%sm%ss"%((deviceID,)+ time.gmtime()[:6])
    device.dump("%s.uix" % tstamp, False)
    device.screenshot("%s.png" % tstamp)

if __name__ == '__main__':
    args = sys.argv
    capture(args[1])



