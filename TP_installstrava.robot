*** Settings ***

Library           Remote    http://${IP_CONTAINER_DROYDRUNNER}:${PORT_LAAS}     WITH NAME    LaaS
Library           droydclient.robot_plugin.Pilot
Library           Collections
Library           String

Resource          macro.robot


*** Variables ***
${IP_CONTAINER_DROYDRUNNER}    127.0.0.1
${PORT_LAAS}                        5000

${IP_RDT_CTRL}          192.168.100.100
${TOKEN_ACCESS_RDT}                    <MY TOKEN>
${API_HTTPS}                    True


${UIID}                 <MY SERIAL>
# WARNING : c'est le 'serial' du mobile que l'on souhaite utiliser pour le test



${timeout_mobile}              8000

# lancement : export RF_HUB_URL=http://laas_droyrunner:5001
#           : robot web.robot



*** Test Cases ***
Test Install Strava
    Builtin.Log To Console    'Test install strava application'

    # Connect mobile from RDT
    ${id_mobile}     LaaS.connectMobile    ${IP_RDT_CTRL}    ${TOKEN_ACCESS_RDT}    ${UIID}    ${API_HTTPS}

    Open Session    ${id_mobile}


    # press home Key
    Macro Press Home Key    ${id_mobile}
    Builtin.sleep    2


    # raz app if needed
    Macro Stop Running Package
    ...    ${id_mobile}
    ...    com.android.vending
    Builtin.sleep    2

    # launch store application
    Macro Launch Package with activity
    ...    ${id_mobile}
    ...    com.android.vending/com.android.vending.AssetBrowserActivity

    Builtin.sleep    2
    Builtin.Log To Console    'wait for exists'
    Builtin.sleep    2
    # wait for the LinearLayout
#    ${tmp_result}    Wait For Exists    ${id_mobile}    resourceId=com.samsung.android.messaging:id/from    textStartsWith=${originphonenumber}    timeout=10
    ${tmp_result}    Wait For Exists    ${id_mobile}     resourceId=com.android.vending:id/search_box_idle_text    timeout=10

    Builtin.Log To Console    'wait for exists result : '${tmp_result}



    Clear Text
    ...    ${id_mobile}
    ...    resourceId=com.android.vending:id/search_box_idle_text    className=android.widget.ImageView


    Click
    ...    ${id_mobile}
    ...    resourceId=com.android.vending:id/search_box_idle_text

    Macro Input Text
    ...    ${id_mobile}
    ...    strava

    # Clear Text
    # ...    ${id_mobile}
    # ...    resourceId=com.android.vending:id/search_box_idle_text


    # Set Text
    # ...     ${id_mobile}
    # ...     resourceId=com.android.vending:id/search_box_idle_text    className=android.widget.ImageView    input_text=strava

    Macro Press Key
    ...    ${id_mobile}
    ...    66

    ${tmp_result}    Wait For Exists    ${id_mobile}     resourceId=com.android.vending:id/li_title    textStartsWith=Strava GPS    timeout=60

    Should Be True
    ...    ${tmp_result}



    ${tmp_result}    Wait For Exists    ${id_mobile}     className=android.widget.Button    textStartsWith=INSTALLER    timeout=60

    Should Be True
    ...    ${tmp_result}

    Click
    ...    ${id_mobile}
    ...    className=android.widget.Button    textStartsWith=INSTALLER

    Builtin.sleep    20

    ${tmp_result}    Wait For Exists    ${id_mobile}     className=android.widget.Button    textStartsWith=OUVRIR    timeout=60

    Should Be True
    ...    ${tmp_result}

    Click
    ...    ${id_mobile}
    ...    className=android.widget.Button    textStartsWith=OUVRIR

    Builtin.sleep    20
    ${tmp_result}    Wait For Exists    ${id_mobile}     resourceId=com.strava:id/welcome_title    textStartsWith=Bienvenue    timeout=60

    Should Be True
    ...    ${tmp_result}


    Macro Press Home Key
    ...     ${id_mobile}

    Builtin.sleep    10



    [Teardown]    TestTeardown    mob=${UIID}     id_mobile=${id_mobile}

*** Keywords ***
TestTeardown
    [arguments]     ${mob}     ${id_mobile}
    [Documentation]     TestTeardown
    Builtin.Log To Console    'TestTeardown'
    #Screenshot      ${id_mobile}
    Close Session
    LaaS.disconnectMobile    ${IP_RDT_CTRL}    ${TOKEN_ACCESS_RDT}    ${mob}    ${API_HTTPS}









