*** Settings ***

Library           Remote    http://${IP_CONTAINER_DROYDRUNNER}:${PORT_LAAS}     WITH NAME    LaaS
Library           droydclient.robot_plugin.Pilot
Library           Collections
Library           String

*** Variables ***

${IP_CONTAINER_DROYDRUNNER}    localhost
${PORT_LAAS}                        5000

${IP_RDT_CTRL}          192.168.100.100
# WARNING : IP du serveur RDT : rdt.si.francetelecom.fr si local RSC
# WARNING : sur le WWW : 192.168.100.100 , c'est la même en prod et dev, il s'agit de l'adresse interne au OPENVPN
${TOKEN_ACCESS_RDT}                    <MY TOKEN>
${API_HTTPS}                    True

${UIID}                 <MY SERIAL>
${timeout_mobile}              8000

# lancement : export RF_HUB_URL=http://127.0.0.1:5001
#           : robot web.robot

*** Test Cases ***
Test Navigation Web
    Builtin.Log To Console    'Test Navigation Web'

	# Add Adb Key to RDT Ctrl
	# addAdbKey    ${IP_RDT_CTRL}    vincent1.barbier@orange.com

    # Connect mobile from RDT
    ${id_mobile}     LaaS.connectMobile    ${IP_RDT_CTRL}    ${TOKEN_ACCESS_RDT}    ${UIID}     ${API_HTTPS}

    Open Session    ${id_mobile}

    # Ouverture du navigateur par défaut
    #Execute Adb Shell Command    ${id_mobile}    cmd=am start -a android.intent.action.VIEW -d http://www.google.fr

    # Ouverture de Chrome
    Execute Adb Shell Command    ${id_mobile}    cmd=am start -n com.android.chrome/com.google.android.apps.chrome.Main -d http://www.google.fr

    # Recherche du champ de saisie
    ${check}    wait for exists    ${id_mobile}    timeout=${timeout_mobile}    description=Rech.
    Run Keyword If    ${check} == False    Fail    msg=Search field not found

    # Input 'Robot framework' and click Search
    click    ${id_mobile}    description=Rech.
    Execute Adb Shell Command    ${id_mobile}    cmd=input text Robot%sframework
    click    ${id_mobile}    className=android.widget.Button    description=Recherche Google

    # Chargement de la page de résultats
    ${check}    wait for exists    ${id_mobile}    timeout=${timeout_mobile}    descriptionContains=Robot Framework - Wikipedia
    Run Keyword If    ${check} == False    Fail    msg=Google search failed

    # Swipe down
    Builtin.Sleep    1
    Swipe By Coordinates    ${id_mobile}    sx=270    sy=2040    ex=270    ey=1000    steps=50
    Builtin.Sleep    1

    # Récuparation du contenu de la page - méthode 1
    ${info}    native operation    ${id_mobile}    operation=info    modifiers=    descriptionContains=Developer
    Builtin.Log To Console    ${info}

    # Récuparation du contenu de la page - méthode 2
    ${objInfo}    Get Object    ${id_mobile}    packageName=com.android.chrome    className=android.view.View    descriptionContains=Developer
    ${contentObj}    Get Info Of Object    ${id_mobile}    obj=${objInfo}
    ${info}    evaluate    ${contentObj}.get("contentDescription")
    Builtin.Log To Console    ${info}

    # Click sur le lien
    click    ${id_mobile}    descriptionContains=Robot Framework - Wikipedia

    # Chargement de la page wikipedia
    ${check}    wait for exists    ${id_mobile}    timeout=${timeout_mobile}    descriptionContains=Pekka Kl\xe4rck
    Run Keyword If    ${check} == False    Fail    msg=Wikipedia loading failed

    Builtin.Sleep    1

    # Click sur Traduire
    ${check}    wait for exists    ${id_mobile}    timeout=${timeout_mobile}    text=Traduire
    Run Keyword If    ${check} == True    Click    ${id_mobile}    text=Traduire

    # Chargement de la page wikipedia traduite
    ${check}    wait for exists    ${id_mobile}    timeout=${timeout_mobile}    descriptionContains=Développeur
    Run Keyword If    ${check} == False    Fail    msg=Wikipedia translate failed

	[Teardown]    TestTeardown    mob=${UIID}

*** Keywords ***
TestTeardown
    [arguments]     ${mob}
    [Documentation]     TestTeardown
    Builtin.Log To Console    'TestTeardown'
    Close Session
    LaaS.disconnectMobile    ${IP_RDT_CTRL}    ${TOKEN_ACCESS_RDT}    ${mob}







